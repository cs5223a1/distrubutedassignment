

import java.io.Serializable;
import java.util.Date;

public class PlayerInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2456891298377228637L;
	private String host;
	private int port;
	private String playerId;
	private long timeStamp;
	private PlayerType type;
	private boolean isDead;

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public PlayerInfo(String host, int port, String playerId, long timeStamp) {
		super();
		this.host = host;
		this.port = port;
		this.playerId = playerId;
		this.timeStamp = timeStamp;
		this.type = PlayerType.PLAYER;
	}
	
	public PlayerInfo(String host, int port, String playerId) {
		super();
		this.host = host;
		this.port = port;
		this.playerId = playerId;
		this.timeStamp = new Date().getTime();
		this.type = PlayerType.PLAYER;
	}
	
	public PlayerInfo clone() {
		PlayerInfo player = new PlayerInfo(this.host, this.port, this.playerId, this.timeStamp);
		player.setType(this.type);
		return player;
	}
	
	public PlayerType getType() {
		return type;
	}

	public void setType(PlayerType type) {
		this.type = type;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public String getPlayerId() {
		return playerId;
	}

	public long getTimeStamp() {
		return timeStamp;
	}
	
	@Override
	public boolean equals(Object playerObj) {
		if(playerObj == null) {
			return false;
		}
		PlayerInfo player = null;
		if(playerObj instanceof PlayerInfo) {
			player =  (PlayerInfo) playerObj;
		}
		return this.playerId.equals(player.getPlayerId()) && 
			   this.port ==  player.port &&
			   this.host.equals(player.host);
	}
}
