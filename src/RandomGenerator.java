

import java.awt.Point;
import java.util.List;
import java.util.Random;

public class RandomGenerator {

	private Random random;
	
	public RandomGenerator() {
		random = new Random();
	}

	public Point getRandomPoint(int n) {
		return new Point(getRandomWithinN(n), getRandomWithinN(n));
	}

	private int getRandomWithinN(int n) {
		return random.nextInt(n);
	}

	public Point getRandomPoint(List<Point> positions) {
		return positions.get(random.nextInt(positions.size()));
	}
}
