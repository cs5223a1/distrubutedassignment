import static org.junit.Assert.*;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;


public class GameLogicTest extends GameLogic {

	public GameLogicTest() {
		super(new GameState( gridSize, treasures, null));
		// TODO Auto-generated constructor stub
	}
	
	private static final int gridSize = 5;
	private static final int treasures = 3;
	private static final int gamePort = 2016;
	private static final int backUpPort = 2017;
	
	private static final String PLAYER1_ID = "P1";
	private static final String PLAYER2_ID = "P2";
	private static final String PLAYER3_ID = "P3";
	private static final String PLAYER4_ID = "P4";
	private static final String PLAYER5_ID = "P5";
	
	private GameState gameState;
	
	
	@BeforeClass
	public static void setup() {
	}
	
	@Test
	public void testAddPlayer() {
		addPlayer(PLAYER1_ID);
		addPlayer(PLAYER2_ID);
		int playerCount = 2;
		gameState = this.getGameState();
		assertEquals(playerCount, gameState.playerPosMap.size());
		assertEquals(0, gameState.availablePositions.size());
		assertEquals(0, gameState.playerScoreMap.get(PLAYER1_ID).intValue());
		
	}
	
	@Test
	public void testRemovePlayer() {
		GameState state = new GameState(15, 10, new HashSet<Point>());
		GameLogic logic = new GameLogic(state);
		logic.addPlayer(PLAYER1_ID);
		logic.addPlayer(PLAYER2_ID);
		int playerCount = 2;
		gameState = logic.getGameState();
		assertEquals(playerCount, gameState.playerPosMap.size());
		assertEquals(0, gameState.availablePositions.size());
		assertEquals(0, gameState.playerScoreMap.get(PLAYER1_ID).intValue());
		
		//logic.removePlayer(new HashSet<String>(){{add(PLAYER2_ID);}});
		logic.removePlayer(PLAYER1_ID);
		playerCount = 1;
		gameState = logic.getGameState();
		assertEquals(playerCount, gameState.playerPosMap.size());
		assertEquals(0, gameState.availablePositions.size());
		assertEquals(0, gameState.playerScoreMap.get(PLAYER2_ID).intValue());
		
		//logic.removePlayer(new HashSet<String>(){{add(PLAYER2_ID);add(PLAYER3_ID);}});
		logic.removePlayer(PLAYER3_ID);
		playerCount = 1;
		gameState = logic.getGameState();
		assertEquals(playerCount, gameState.playerPosMap.size());
		assertEquals(0, gameState.availablePositions.size());
		assertEquals(0, gameState.playerScoreMap.get(PLAYER2_ID).intValue());
		
		
		
	}
	
	@Test
	public void testMultiPlayerMoves() {
		addPlayer(PLAYER1_ID);
		addPlayer(PLAYER2_ID);
		addPlayer(PLAYER3_ID);
		int playerCount = 3;
		gameState = this.getGameState();
		Set<Point> oldPlayerPos = gameState.getPlayerPositions();
		Set<Point> oldTreasureLoc = gameState.getTreasureLocations();
		Map<String,Integer> oldScoresMap = gameState.getPlayerScoreMap();
		
		for(String player : new String[]{PLAYER1_ID, PLAYER2_ID, PLAYER3_ID}) {
			for(int i=1; i <= 4; i++){
				int oldScores = 0, newScores = 0;
				GameMove move = GameMove.getGameMode(Integer.toString(i));
				updatePlayerLocation(move, player);
				
				gameState = this.getGameState();
				Set<Point> newPlayerPos = gameState.getPlayerPositions();
				Set<Point> newTreasureLoc = gameState.getTreasureLocations();
				Map<String,Integer> newScoresMap = gameState.getPlayerScoreMap();
				
				for (int f : oldScoresMap.values()) {
				    oldScores += f;
				}
				
				for (int f : newScoresMap.values()) {
				    newScores += f;
				}
				
				Set<Point> treasuresTakenLoc = new HashSet<Point>(newPlayerPos); 
				treasuresTakenLoc.retainAll(oldTreasureLoc);
				
				int scoreDiff = newScores - oldScores;
				
				System.out.println(oldPlayerPos);
				System.out.println(oldTreasureLoc);
				System.out.println(treasuresTakenLoc);
				System.out.println(newPlayerPos);
				System.out.println(newTreasureLoc);
				System.out.println("........");
				
				if( treasuresTakenLoc.size() > 0 && treasuresTakenLoc != null ){
					assertTrue("Success: Scores added", scoreDiff == 1 );
					assertEquals(false, newTreasureLoc.removeAll(treasuresTakenLoc));
				}
				else {
					assertEquals(newTreasureLoc, oldTreasureLoc);
				}
				oldPlayerPos = newPlayerPos;
				oldTreasureLoc = newTreasureLoc;
				oldScoresMap = newScoresMap;
			}
		}
		System.out.println(gameState.getPlayerPositions());
		assertEquals(0, gameState.availablePositions.size());
	}

	@Test
	public void TestSinglePlayerMoves() {
		addPlayer(PLAYER1_ID);
		
		gameState = this.getGameState();
		Point oldPos = gameState.getPlayerPosiMap().get(PLAYER1_ID);
		
		for(int i=1; i <= 4; i++){
			GameMove move = GameMove.getGameMode(Integer.toString(i));
			updatePlayerLocation(move, PLAYER1_ID);
			
			gameState = this.getGameState();
			Point newPos = gameState.getPlayerPosiMap().get(PLAYER1_ID);
		
			if ( i == 1 && oldPos.x > 0) {
				oldPos.x -= 1;
			} else if (i == 2 && oldPos.y < gridSize-1) {
				oldPos.y += 1;
			} else if ( i == 3 && oldPos.x < gridSize-1)  {
				oldPos.x += 1;
			} else if ( i ==4 && oldPos.y > 0) {
				oldPos.y -= 1;
			}
			assertEquals(oldPos.x, newPos.x);
			assertEquals(oldPos.y, newPos.y);

		}
		
	}
	
	
}
