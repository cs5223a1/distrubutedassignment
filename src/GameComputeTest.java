

import static org.junit.Assert.*;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;


public class GameComputeTest extends Game {

	public GameComputeTest() {
		super(PlayerType.PLAYER);
	}
	
	private static final int gridSize = 15;
	private static final int treasures = 10;
	private static final int trackerPort = 2000;
	private static final int gamePort = 2016;
	private static final int backUpPort = 2017;
	
	private static final String PLAYER1_ID = "P1";
	private static final String PLAYER2_ID = "P2";
	private static final String PLAYER3_ID = "P3";
	private static final String PLAYER4_ID = "P4";
	private static final String PLAYER5_ID = "P5";
	
	String ip = "";
	String gameProg = "Game";
	
	private static ITracker tracker;
	
	@BeforeClass
	public static void setup() {
		try {
			Registry register = LocateRegistry.getRegistry(trackerPort);
			tracker = (ITracker)register.lookup("tracker");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Before
	public void cleanTest() {
		try {
			tracker.removeAllPlayers();
			unbind(gamePort);
			unbind(backUpPort);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void unbind(int port) {
		Registry registry = null;
		try {
			registry = java.rmi.registry.LocateRegistry.createRegistry(port);
			UnicastRemoteObject.unexportObject(registry, true);
		} catch (ExportException e) {
			try {
				registry = LocateRegistry.getRegistry(port);
				registry.unbind("game");
				System.out.println("[Game RMI] Port killed : " + port);
			} catch (RemoteException e1) {
				System.out.println("[Game Test] Remote Exception " + e1.getMessage());
			} catch (NotBoundException e1) {
				System.out.println("[Game Test] Not Bound Exception " + e1.getMessage() );
			}
		} catch (RemoteException e) {
			System.out.println("[Game Test] Remote Exception " + e.getMessage());
		}
		
	}
	
	public boolean testPortRunning(int port) {
		Registry registry = null;
		try {
			registry = java.rmi.registry.LocateRegistry.getRegistry(port);
			registry.lookup("game");
			return true;
		} catch (RemoteException e) {
			System.out.println("[Game Test] Remote Exception " + e.getMessage());
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Test
	public void testSinglePlayerJoins() {
		System.out.println("\n[###Test###] - testSinglePlayerJoins\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state = player1.init(ip, trackerPort, PLAYER1_ID);
		
		int playerCount = 1;
		//GameStateUtil.printGameState(state);
		assertEquals(gridSize, state.getN());
		assertEquals(treasures, state.getK());
		assertEquals(treasures, state.treasureLoc.size());
		
		/*Availble positions are computed as each node. Not copied and send in the game state*/
		assertEquals(0, state.availablePositions.size());
		assertEquals(playerCount, state.playerPosMap.size());
		assertEquals(playerCount, state.playerScoreMap.size());
		assertEquals(playerCount, state.getPlayerPosiMap().size());
		assertEquals(playerCount, state.getPosiToPlayerMap().size());
		
		assertEquals("P1", state.getPrimaryServer().getPlayerId());
	}
	
	@Test
	public void testGameAfterBothPrimaryBackupDies() {
		System.out.println("\n[###Test###] - testSinglePlayerJoins\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		unbind(gamePort);
		unbind(backUpPort);

		Game player3 = new Game(PlayerType.PLAYER);
		GameState state3 = player3.init(ip, trackerPort, PLAYER3_ID);
		assertEquals(PLAYER3_ID, state3.getPrimaryServer().getPlayerId());
		assertEquals(null, state3.getBackUpServer());
		
		state3 = player3.makeMove("4", PLAYER3_ID);
		assertEquals(PLAYER3_ID, state3.getPrimaryServer().getPlayerId());
		assertEquals(null, state3.getBackUpServer());
		
		Game player4 = new Game(PlayerType.PLAYER);
		GameState state4 = player4.init(ip, trackerPort, PLAYER4_ID);
		assertEquals(PLAYER3_ID, state4.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER4_ID, state4.getBackUpServer().getPlayerId());
		
		state4 = player4.makeMove("4", PLAYER3_ID);
		assertEquals(PLAYER3_ID, state4.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER4_ID, state4.getBackUpServer().getPlayerId());
		
		state3 = player3.makeMove("4", PLAYER3_ID);
		assertEquals(PLAYER3_ID, state3.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER4_ID, state3.getBackUpServer().getPlayerId());
		
		state4 = player4.makeMove("4", PLAYER3_ID);
		assertEquals(PLAYER3_ID, state4.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER4_ID, state4.getBackUpServer().getPlayerId());
		
	}
	
	@Test
	public void testTwoPlayerJoins() {
		System.out.println("\n[###Test###] - testTwoPlayerJoins\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		
		int playerCount = 2;
		//GameStateUtil.printGameState(state);
		assertEquals(gridSize, state1.getN());
		assertEquals(treasures, state1.getK());
		assertEquals(treasures, state1.treasureLoc.size());
		
		/*Availble positions are computed as each node. Not copied and send in the game state1*/
		assertEquals(0, state2.availablePositions.size());
		assertEquals(playerCount, state2.playerPosMap.size());
		assertEquals(playerCount, state2.playerScoreMap.size());
		assertEquals(playerCount, state2.getPlayerPosiMap().size());
		assertEquals(playerCount, state2.getPosiToPlayerMap().size());

		Registry register;
		Game game = null;
		try {
			register = LocateRegistry.getRegistry(gamePort);
			IGame temp = (IGame) register.lookup("game");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//TODO FIX ME
		assertEquals(PLAYER1_ID, state1.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		
	}
	
	@Test
	public void testWhenBackupTriesBecomePrimary() {
		System.out.println("\n[###Test###] - testWhenBackupTriesBecomePrimary\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		player1.unbind(gamePort);
		
		state2 = player2.makeMove("4", PLAYER2_ID);
		int playerCount = 2;
		assertEquals(gridSize, state2.getN());
		assertEquals(treasures, state2.getK());
		assertEquals(treasures, state2.treasureLoc.size());
		
		/*Availble positions are computed as each node. Not copied and send in the game state1*/
		assertEquals(0, state2.availablePositions.size());
		assertEquals(playerCount, state2.playerPosMap.size());
		assertEquals(playerCount, state2.playerScoreMap.size());
		assertEquals(playerCount, state2.getPlayerPosiMap().size());
		assertEquals(playerCount, state2.getPosiToPlayerMap().size());

		//TODO FIX ME
		assertEquals(PLAYER1_ID, state2.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		
	}
	
	@Ignore
	public void testPrimaryDiesB4BackIsCreated() {
		System.out.println("\n[###Test###] - testPrimaryDies\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		player1.unbind(gamePort);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		
		int playerCount = 1;
		//GameStateUtil.printGameState(state);
		assertEquals(gridSize, state1.getN());
		assertEquals(treasures, state1.getK());
		assertEquals(treasures, state1.treasureLoc.size());
		
		/*Availble positions are computed as each node. Not copied and send in the game state1*/
		assertEquals(0, state2.availablePositions.size());
		assertEquals(playerCount, state2.playerPosMap.size());
		assertEquals(playerCount, state2.playerScoreMap.size());
		assertEquals(playerCount, state2.getPlayerPosiMap().size());
		assertEquals(playerCount, state2.getPosiToPlayerMap().size());
		
		assertEquals(PLAYER2_ID, state2.getPrimaryServer().getPlayerId());
	}
	
	@Test
	public void testPrimaryDiesJustB4Player3Joins() {
		System.out.println("\n[###Test###] - testPrimaryDiesJustB4Player3Joins\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		player1.unbind(gamePort);
		player1 = null;
		assertEquals(true, testPortRunning(backUpPort));
		Game player3 = new Game(PlayerType.PLAYER);
		GameState state3 = player3.init(ip, trackerPort, PLAYER3_ID);
		
		//Here the dead primary player is left alive
		int playerCount = 3;
		//GameStateUtil.printGameState(state);
		assertEquals(gridSize, state3.getN());
		assertEquals(treasures, state3.getK());
		assertEquals(treasures, state3.treasureLoc.size());
		
		/*Availble positions are computed as each node. Not copied and send in the game state1*/
		assertEquals(0, state3.availablePositions.size());
		assertEquals(playerCount, state3.playerPosMap.size());
		assertEquals(playerCount, state3.playerScoreMap.size());
		assertEquals(playerCount, state3.getPlayerPosiMap().size());
		assertEquals(playerCount, state3.getPosiToPlayerMap().size());
		
		assertEquals(PLAYER3_ID, state3.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state3.getBackUpServer().getPlayerId());
		assertEquals(PLAYER1_ID, state2.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		
		state2 = player2.makeMove("0", PLAYER2_ID);
		assertEquals(PLAYER3_ID, state2.getPrimaryServer().getPlayerId());
		
	}
	
	@Test
	public void testPrimaryDiesJustB4Player4Joins() {
		System.out.println("\n[###Test###] - testPrimaryDiesJustB4Player4Joins\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		Game player3 = new Game(PlayerType.PLAYER);
		GameState state3 = player3.init(ip, trackerPort, PLAYER3_ID);
		player1.unbind(gamePort);
		assertEquals(true, testPortRunning(backUpPort));
		Game player4 = new Game(PlayerType.PLAYER);
		GameState state4 = player4.init(ip, trackerPort, PLAYER4_ID);
		state3 = player3.makeMove("0", PLAYER3_ID);
		
		//Here the dead primary player is left alive
		int playerCount = 4;
		//GameStateUtil.printGameState(state);
		assertEquals(gridSize, state3.getN());
		assertEquals(treasures, state3.getK());
		assertEquals(treasures, state3.treasureLoc.size());
		
		/*Availble positions are computed as each node. Not copied and send in the game state1*/
		assertEquals(0, state3.availablePositions.size());
		assertEquals(playerCount, state3.playerPosMap.size());
		assertEquals(playerCount, state3.playerScoreMap.size());
		assertEquals(playerCount, state3.getPlayerPosiMap().size());
		assertEquals(playerCount, state3.getPosiToPlayerMap().size());
		
		assertEquals(PLAYER4_ID, state3.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state3.getBackUpServer().getPlayerId());
		
		state2 = player2.makeMove("0", PLAYER2_ID);
		assertEquals(PLAYER4_ID, state2.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		
		assertEquals(0, state3.availablePositions.size());
		assertEquals(playerCount, state2.playerPosMap.size());
		assertEquals(playerCount, state2.playerScoreMap.size());
		assertEquals(playerCount, state2.getPlayerPosiMap().size());
		assertEquals(playerCount, state2.getPosiToPlayerMap().size());
		
		state4 = player4.makeMove("0", PLAYER4_ID);
		assertEquals(PLAYER4_ID, state4.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state4.getBackUpServer().getPlayerId());
		
		assertEquals(0, state4.availablePositions.size());
		assertEquals(playerCount, state4.playerPosMap.size());
		assertEquals(playerCount, state4.playerScoreMap.size());
		assertEquals(playerCount, state4.getPlayerPosiMap().size());
		assertEquals(playerCount, state4.getPosiToPlayerMap().size());
		
	}
	
	@Test
	public void testPrimaryDiesJustB4Player3Moves() {
		System.out.println("\n[###Test###] - testPrimaryDiesJustB4Player3Moves\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		Game player3 = new Game(PlayerType.PLAYER);
		GameState state3 = player3.init(ip, trackerPort, PLAYER3_ID);
		player1.unbind(gamePort);
		state3 = player3.makeMove(Short.toString(GameMove.EAST.getCode()), PLAYER3_ID);
		
		//Here the dead primary player is left alive
		int playerCount = 3;
		//GameStateUtil.printGameState(state);
		assertEquals(gridSize, state3.getN());
		assertEquals(treasures, state3.getK());
		assertEquals(treasures, state3.treasureLoc.size());
		
		/*Availble positions are computed as each node. Not copied and send in the game state1*/
		assertEquals(0, state3.availablePositions.size());
		assertEquals(playerCount, state3.playerPosMap.size());
		assertEquals(playerCount, state3.playerScoreMap.size());
		assertEquals(playerCount, state3.getPlayerPosiMap().size());
		assertEquals(playerCount, state3.getPosiToPlayerMap().size());
		
		assertEquals(PLAYER3_ID, state3.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state3.getBackUpServer().getPlayerId());
		
		assertEquals(PLAYER1_ID, state1.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		
		
		state2 = player2.makeMove(Short.toString(GameMove.EAST.getCode()), PLAYER2_ID);
		
		assertEquals(PLAYER3_ID, state2.getPrimaryServer().getPlayerId());
		assertEquals(PLAYER2_ID, state2.getBackUpServer().getPlayerId());
		
		
	}
	
	@Test
	public void testBackupCreationOnceAfterDead() {
		System.out.println("\n[###Test###] - testBackupCreationOnceAfterDead\n");
		Game player1 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		Game player2 = new Game(PlayerType.PLAYER);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		player2.unbind(backUpPort);
		Game player3 = new Game(PlayerType.PLAYER);
		GameState state3 = player3.init(ip, trackerPort, PLAYER3_ID);
		assertEquals(PLAYER3_ID, state3.getBackUpServer().getPlayerId());
		state3 = player3.makeMove(Short.toString(GameMove.EAST.getCode()), PLAYER3_ID);
		assertEquals(PLAYER3_ID, state3.getBackUpServer().getPlayerId());
		
	}
	
	@Test
	public void testMultipleCreationPrimaryServers() {
		System.out.println("\n[###Test###] - testMultipleCreationPrimaryServers\n");
		Game player1 = new Game(PlayerType.PLAYER);
		Game player2 = new Game(PlayerType.PLAYER);
		Game player3 = new Game(PlayerType.PLAYER);
		Game player4 = new Game(PlayerType.PLAYER);
		Game player5 = new Game(PlayerType.PLAYER);
		GameState state1 = player1.init(ip, trackerPort, PLAYER1_ID);
		GameState state2 = player2.init(ip, trackerPort, PLAYER2_ID);
		GameState state3 = player3.init(ip, trackerPort, PLAYER3_ID);
		GameState state4 = player4.init(ip, trackerPort, PLAYER4_ID);
		GameState state5 = player5.init(ip, trackerPort, PLAYER5_ID);
		
		
	}
	
	
	
	

	private int calcAvaliablePositions(GameState state3, int playerCount) {
		return state3.getN() * state3.getN() - state3.getK() - playerCount;
	}

}