
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Game implements IGame {

	private final int gamePort = 2016;
	private final int backUpPort = 2017;
	private GameLogic gameLogic;
	private PlayerInfo player;
	private static MazeUI mazeUI;
	private String host = "";

	/**
	 * This should be pointing to Game object from primary
	 */

	private int gridSize;
	private int treasures;

	public Game(int gridSize, int treasures, PlayerType type) {
		this.gridSize = gridSize;
		this.treasures = treasures;
	}

	public Game(PlayerType type) {
	}


	/*
	 * True : if the primary server was found.
	 */


	private int createGameServer(String host) {

		Registry registry = null;
		int playerPort = 0;
		boolean created = false;
		while(created == false)
		{
			try
			{
				try {
					playerPort = 2050 + (int)(Math.random()*5000);
					registry = java.rmi.registry.LocateRegistry.createRegistry(playerPort);
				} catch(ExportException e) {
				}
				Game server = new Game(this.gridSize, this.treasures, PlayerType.PLAYER);
				IGame stub = (IGame) UnicastRemoteObject.exportObject(server, 0);
				registry.bind("player", stub);
				created = true;
			} catch (RemoteException e) {
				e.printStackTrace();
				created = false;
			} catch (AlreadyBoundException e) {
				e.printStackTrace();
				created = false;
			}		
		}
		return playerPort;
	}



	protected GameState init(String host, int port, String playerId) {
		TrackerInfo trinfo = null;
		ITracker t = null;
		List<PlayerInfo> players = null;
		int playerPort = createGameServer(host);
		//connect Tracker to get player list
		try
		{			
			t = (ITracker)Naming.lookup("rmi://"+host+":"+port+"/tracker");
			this.player = new PlayerInfo(host, playerPort, playerId, new Date().getTime());
			trinfo = t.addAndFetchPlayers(this.player);
			synchronized (trinfo) {
				gridSize = trinfo.getN();
				treasures = trinfo.getK();
				players = trinfo.getPlayers();
			}
		}
		catch(RemoteException e)
		{} catch (NotBoundException e) {
		} catch (MalformedURLException e) {
		} 

		GameState result = joinPrimary(this.player, players, false);

		if(result != null) {
			mazeUI = new MazeUI(result,playerId);
			return result;
		}
		result = joinBackup(this.player, players);
		if(result != null) {
			mazeUI = new MazeUI(result,playerId);
			return result;
		}

		//worst case
		makePrimaryServer();
		result = joinPrimary(this.player, players, true);
		mazeUI = new MazeUI(result,playerId);
		return result;
	}

	private void makeBackupServer(PlayerInfo player, GameState gameState) {
		Registry registry = null;
		try
		{
			try {
				registry = java.rmi.registry.LocateRegistry.createRegistry(backUpPort);
			} catch(ExportException e) {
			}
			try {
				Naming.lookup("rmi://"+host+":"+backUpPort+"/game");
				return;
			} catch (NotBoundException e) {
			} catch (MalformedURLException e) {
			} 
			Game server = new Game(this.gridSize, this.treasures, PlayerType.BACKUP_SERVER);
			server.copyState(gameState);
			server.gameLogic.updateBackupServer(player);
			IGame stub = (IGame) UnicastRemoteObject.exportObject(server, 0);
			registry.bind("game", stub);
		} catch (RemoteException e) {
		} catch (AlreadyBoundException e) {
		}
	}

	private synchronized GameState joinAndUpdatePlayers(PlayerInfo player, List<PlayerInfo> playerInfos) {
		GameState gameState = this.gameLogic.addPlayer(player.getPlayerId());	
		Set<String> inStatePlayerIds = new HashSet(gameState.playerPosMap.keySet());
		for(PlayerInfo dead : playerInfos) {
			if(dead.isDead() && !checkPlayerAlive(dead)) {
				this.gameLogic.removePlayer(dead.getPlayerId());
			}
		}
		if(this.gameLogic.getBackUpServer() == null && this.gameLogic.getPrimaryServer() != null && !player.equals(this.gameLogic.getPrimaryServer())) {
			this.gameLogic.updateBackupServer(new PlayerInfo(player.getHost(), player.getPort(), player.getPlayerId()));
		}
		updateBackupServer(this.gameLogic.getGameState(), player);
		return this.gameLogic.getGameState();
	}

	private void updateBackupServer(GameState gameState, PlayerInfo player) {
		try {			
			IGame backupGame = (IGame)Naming.lookup("rmi://"+host+":"+backUpPort+"/game");
			backupGame.copyState(gameState);
		} catch (RemoteException e) {
			if(!player.getPlayerId().equals(this.gameLogic.getPrimaryServer().getPlayerId())) {
				this.gameLogic.updateBackupServer(player);
			}
		} catch (NotBoundException e) {
			if(!player.getPlayerId().equals(this.gameLogic.getPrimaryServer().getPlayerId())) {
				this.gameLogic.updateBackupServer(player);
			}
		} catch (MalformedURLException e) {
		}

	}

	@Override
	public synchronized GameState joinPrimaryAndUpdatePlayers(PlayerInfo server, PlayerInfo player, List<PlayerInfo> deadPlayerIds) {
		GameState result =  joinAndUpdatePlayers(player, deadPlayerIds);
		return result;
	}

	@Override
	public synchronized GameState joinBackupAndUpdatePlayers(PlayerInfo server, PlayerInfo player, List<PlayerInfo> deadPlayerIds) {
		PlayerInfo backUpPlayer = this.gameLogic.getGameState().getBackUpServer();
		if(backUpPlayer != null && !backUpPlayer.getPlayerId().equals(server.getPlayerId())) {
			return null;
		}
		return joinAndUpdatePlayers(player, deadPlayerIds);
	}

	private GameState joinPrimaryGame(PlayerInfo server, List<PlayerInfo> allPlayers) throws RemoteException, NotBoundException, MalformedURLException {
		IGame game = (IGame)Naming.lookup("rmi://"+host+":"+gamePort+"/game");
		GameState result = game.joinPrimaryAndUpdatePlayers(server, this.player, allPlayers);
		if(result.getBackUpServer() == null ) {
			return result;
		}
		if(result.getBackUpServer().getPlayerId().equals(player.getPlayerId())) {
			makeBackupServer(player, result);
		}
		return result;
	}


	private GameState joinBackupGame(PlayerInfo server, List<PlayerInfo> allPlayers) throws RemoteException, NotBoundException, MalformedURLException {
		IGame game = (IGame)Naming.lookup("rmi://"+host+":"+backUpPort+"/game");
		GameState state = game.joinBackupAndUpdatePlayers(server, this.player, allPlayers);
		if(state != null && state.getBackUpServer().getPlayerId().equals(player.getPlayerId())) {
			makeBackupServer(server, state);
		}
		return state;
	}


	private GameState joinBackup(PlayerInfo currentPlayer, List<PlayerInfo> players) {
		GameState g = null;
		List<PlayerInfo> tempList = new ArrayList<PlayerInfo>(players.size());
		for(PlayerInfo player : players) {
			tempList.add(player.clone());
		}
		if(players != null && players.size() > 0) {
			for (PlayerInfo p : players) {
				try {
					g = joinBackupGame(p, tempList);
				} catch (java.rmi.ConnectException e) {
				} catch (RemoteException e) {
				} catch (NotBoundException e) {
				} catch (MalformedURLException e) {
				}
				if (g != null) {
					PlayerInfo temp = g.getPrimaryServer();
					g.setPrimaryServer(currentPlayer);
					if(!makePrimaryServer(g, p)) {
						g.setPrimaryServer(temp);
						return joinPrimary(currentPlayer, players, false);
					} else {
					}
					return g;
				}
			}
		}
		return g;
	}

	private GameState joinPrimary(PlayerInfo currentPlayer, List<PlayerInfo> players, boolean allDead) {
		GameState g = null;
		if(players.size() == 1)
		{
			try {
				makePrimaryServer();
			} catch (Exception e) {
			}
		}
		List<PlayerInfo> temp = new ArrayList<PlayerInfo>(players);
		if(players != null && players.size() > 0) {
			for (PlayerInfo p : players) {
				try {
					if(p.getPlayerId() != currentPlayer.getPlayerId())
						g = joinPrimaryGame(p, temp);
				} catch (java.rmi.ConnectException e) {
				} catch (RemoteException e) {
				} catch (NotBoundException e) {
				} catch (MalformedURLException e) {
				}
				if (g != null) {
					return g;
				}
			}
		}
		return g;
	}



	public static void main(String[] args) {
		Game player1 = new Game(5,4,PlayerType.PLAYER);
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String playerId = args[2];
		GameState state = player1.init(host, port, playerId);
		if(state == null ) {
			return;
		}
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				String input = br.readLine().trim();
				state = player1.makeMove(input, playerId);
				player1.refreshCurrentPlayer(state);
			}
		} catch (Exception e) {
		}
	}


	@Override
	public void copyState(GameState gameState) {
		if(gameState == null) {
			return;
		}
		if(this.gameLogic == null) {
			this.gameLogic = new GameLogic(gameState);
		} else {
			this.gameLogic.setGameState(gameState);

		}
	}

	
	@Override
	public synchronized GameState move(String input, String playerId) {
		GameMove move = GameMove.getGameMode(input);
		GameState state;
		if (move == GameMove.QUIT) {
			this.gameLogic.quitGame(playerId);
			updateBackupServer(this.gameLogic.getGameState(), new PlayerInfo("", 0, playerId));
			state = this.gameLogic.getGameState();
		} else if (move == GameMove.REFRESH) {
			state = this.gameLogic.getGameState();
		} else {
			this.gameLogic.updatePlayerLocation(move, playerId);
			updateBackupServer(this.gameLogic.getGameState(), new PlayerInfo("", 0, playerId));
			state = this.gameLogic.getGameState();
		}
		return state;
	}

	@Override
	public synchronized void makePrimaryServer() {

		try {
			Registry registry = null;
			try {
				registry = java.rmi.registry.LocateRegistry.createRegistry(gamePort);
			} catch (ExportException e) {
			}
			Game server = new Game(this.gridSize, this.treasures, PlayerType.PRIMARY_SERVER);
			server.createGame();
			server.gameLogic.updatePrimaryServer(new PlayerInfo(player.getHost(), player.getPort(), player.getPlayerId()));
			IGame stub = (IGame) UnicastRemoteObject.exportObject(server, 0);
			registry.bind("game", stub);
		} catch (RemoteException e) {
		} catch (AlreadyBoundException e) {
		}
	}

	public GameState makeMove(String input, String playerId) {
		try {
			GameState state  = fetchGameRemoteObj(gamePort).move(input, playerId);
			if(state.getBackUpServer() != null && state.getBackUpServer().getPlayerId().equals(playerId)) {
				makeBackupServer(new PlayerInfo("", 0, playerId), state);
			}
			return state;
		} catch (RemoteException e) {
			IGame game = createNewPrimary();
			try {
				return game.move(input, playerId);
			} catch (RemoteException e1) {
			}
		} catch (NotBoundException e) {
			IGame game = createNewPrimary();
			try {
				return game.move(input, playerId);
			} catch (RemoteException e1) {
			}
		} 
		return null;
	}

	private IGame createNewPrimary() {
		GameState g = getGameStateFromBackUp();
		if(!g.getBackUpServer().getPlayerId().equals(this.player.getPlayerId())) {
			makePrimaryServer(g, g.getBackUpServer());
		}
		try {
			return fetchGameRemoteObj(gamePort);
		} catch (NotBoundException e) {
			try {
				return fetchGameRemoteObj(backUpPort);
			} catch (NotBoundException e1) {
			}
		}
		return null;
	}

	public GameState getGameStateFromBackUp() {
		try {
			IGame game = (IGame)Naming.lookup("rmi://"+host+":"+backUpPort+"/game");
			return game.move(Short.toString(GameMove.REFRESH.getCode()), "");
		} catch (RemoteException e) {
		} catch (NotBoundException e) {
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private IGame fetchGameRemoteObj(int port) throws NotBoundException {
		Registry register;
		boolean primaryAlive = true;
		try {
			IGame game = (IGame)Naming.lookup("rmi://"+host+":"+port+"/game");
			return game;
		} catch (RemoteException e) {
			primaryAlive = false;
		} catch (MalformedURLException e) {
		}
		if(!primaryAlive) {
			return createNewPrimary();
		}
		return null;
	}

	private boolean checkPlayerAlive(PlayerInfo player) {
		Registry register;
		try {
			IGame game = (IGame)Naming.lookup("rmi://"+host+":"+player.getPort()+"/player");
		} catch (RemoteException e) {
			return false;
		} catch (NotBoundException e) {
			return false;
		} catch (MalformedURLException e) {
		}
		return true;
	}


	private GameState createGame() {
		gameLogic = new GameLogic(new GameState(gridSize, treasures, null));
		return gameLogic.getGameState();
	}

	@Override
	public synchronized boolean makePrimaryServer(GameState gameState, PlayerInfo backUpServer) {

		try {
			Registry registry = null;
			try {
				registry = java.rmi.registry.LocateRegistry.createRegistry(gamePort);
			} catch (ExportException e) {
			}
			Game server = new Game(this.gridSize, this.treasures, PlayerType.PRIMARY_SERVER);
			server.copyState(gameState);
			server.gameLogic.updatePrimaryServer(new PlayerInfo(player.getHost(), player.getPort(), player.getPlayerId()));
			server.gameLogic.updateBackupServer(backUpServer);
			IGame stub = (IGame) UnicastRemoteObject.exportObject(server, 0);
			registry.rebind("game", stub);
			updateBackupServer(server.gameLogic.getGameState(), backUpServer);
		} catch (RemoteException e) {
			return false;
		}
		return true;
	}

	private void refreshCurrentPlayer(GameState gameState)
	{	
		try
		{
			mazeUI.refresh(gameState);
		}
		catch(Exception e)
		{}
	}

	@Override
	public void unbind(int port) {
		Registry registry = null;
		try {
			registry = java.rmi.registry.LocateRegistry.createRegistry(port);
			UnicastRemoteObject.unexportObject(registry, true);
		} catch (ExportException e) {
			try {
				Naming.lookup("rmi://"+host+":"+port+"/game");
				System.out.println("[Game RMI] Port killed : " + port);
			} catch (RemoteException e1) {
			} catch (NotBoundException e1) {
			} catch (MalformedURLException e1) {
			}
		} catch (RemoteException e) {
		}

	}
}