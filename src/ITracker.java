

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

/**
 * Tracker maintains a list of active players
 */
public interface ITracker extends Remote {

	/**
	 * 
	 * 
	 * 1. Tracker will register the new player with the playerInfo data.
	 * 2. returns the list of currents players to the given count
	 * 
	 * @param host
	 * @param port
	 * @param playerId
	 * @return
	 */
	
	public void init(int port,int n,int k) throws RemoteException;
	
	TrackerInfo addAndFetchPlayers(PlayerInfo playerinfo) throws RemoteException;
	
	/**
	 * 
	 * Players with given ids are removed from the list maintained by the Tracker.
	 * 
	 * @param playerIds
	 */
	public void removePlayers(Set<String> playerIds) throws RemoteException;
	
	/**
	 * Remove all playeres
	 * @throws RemoteException
	 */
	public void removeAllPlayers() throws RemoteException;

	void updatePlayerList(List<PlayerInfo> players) throws RemoteException;
	
}
