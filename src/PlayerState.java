

import java.awt.Point;

public class PlayerState {

	String playerId;
	private int x;
	private int y;
	private int score;

	public PlayerState(String playerId, int x, int y, int score) {
		super();
		this.x = x;
		this.y = y;
		this.score = score;
		this.playerId = playerId;
	}
	
	public PlayerState(String playerId, Point p, int score) {
		super();
		this.x = p.x;
		this.y = p.y;
		this.score = score;
		this.playerId = playerId;
	}

	@Override
	public PlayerState clone() {
		return new PlayerState(playerId, this.x, this.y, this.score);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getScore() {
		return score;
	}
	
	public String getPlayerId() {
		return playerId;
	}
	
	public Point getPosition() {
		return new Point(x,y);
	}

}
