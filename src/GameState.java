

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GameState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1028673391523190245L;
	int n;
	int k;
	Map<String, Point> playerPosMap;
	Map<String, Integer> playerScoreMap;
	Set<Point> treasureLoc;
	List<Point> availablePositions;
	Map<String,Boolean> treasurePoints;
	PlayerInfo primaryServer;
	PlayerInfo backUpServer;

	public GameState(int n, int k, Set<Point> treasureList) {
		super();
		this.n = n;
		this.k = k;
		if (treasureList != null) {
			this.treasureLoc = treasureList;
		} else {
			this.treasureLoc = new HashSet<Point>();
		}
		playerPosMap = new HashMap<String, Point>();
		playerScoreMap = new HashMap<String, Integer>();
		availablePositions = new ArrayList<Point>();
	}

	public GameState(int n, int k, Map<String, Point> playerPosMap, Map<String, Integer> playerScoreMap,
			Set<Point> treasureLoc) {
		super();
		this.n = n;
		this.k = k;
		this.playerPosMap = playerPosMap;
		this.playerScoreMap = playerScoreMap;
		this.treasureLoc = treasureLoc;
		this.availablePositions = new ArrayList<Point>();
	}

	/**
	 * Note : availablePositions map is not copied, it is meant to be generated
	 * in logic.
	 */
	@Override
	public GameState clone() {

		GameState gameState = new GameState(n, k, this.getPlayerPosiMap(), this.getPlayerScoreMap(), this.getTreasureLocations());
		if(this.primaryServer != null) {
			gameState.setPrimaryServer(this.primaryServer.clone());
		}
		if(this.backUpServer != null) {
			gameState.setBackUpServer(this.backUpServer.clone());
		}
		return gameState;
	}

	public int getN() {
		return n;
	}

	public int getK() {
		return k;
	}

	public Set<Point> getTreasureLocations() {
		Set<Point> treasureLocCopy = new HashSet<Point>();
		for (Point p : treasureLoc) {
			treasureLocCopy.add(new Point(p.x, p.y));
		}
		return treasureLocCopy;
	}
	
	public void setTreasureLocations(Set<Point> treasureLoc) {
		Set<Point> treasureLocCopy = new HashSet<Point>();
		for (Point p : treasureLoc) {
			treasureLocCopy.add(new Point(p.x, p.y));
		}
		this.treasureLoc = treasureLocCopy;
	}

	public Set<Point> getPlayerPositions() {
		Set<Point> copyList = new HashSet<Point>();
		for (Point posi : playerPosMap.values()) {
			copyList.add(new Point(posi.x, posi.y));
		}
		return copyList;
	}

	public Map<String, Point> getPlayerPosiMap() {
		Map<String, Point> posMapCopy = new HashMap<String, Point>();
		for (String playerId : playerPosMap.keySet()) {
			Point p = playerPosMap.get(playerId);
			posMapCopy.put(playerId, new Point(p.x, p.y));
		}
		return posMapCopy;
	}

	public Map<String, Integer> getPlayerScoreMap() {
		Map<String, Integer> scoreMapCopy = new HashMap<String, Integer>();
		for (String playerId : playerScoreMap.keySet()) {
			scoreMapCopy.put(playerId, playerScoreMap.get(playerId));
		}
		return scoreMapCopy;
	}

	public Map<Point, String> getPosiToPlayerMap() {
		Map<Point, String> copyMap = new HashMap<Point, String>();
		for (String key : playerPosMap.keySet()) {
			copyMap.put(playerPosMap.get(key), key);
		}
		return copyMap;
	}

	public PlayerInfo getPrimaryServer() {
		return primaryServer;
	}

	public void setPrimaryServer(PlayerInfo primaryServer) {
		this.primaryServer = primaryServer.clone();
	}

	public PlayerInfo getBackUpServer() {
		return backUpServer;
	}

	public void setBackUpServer(PlayerInfo backUpServer) {
		this.backUpServer = backUpServer;
	}
	
	public void removePlayer(String playerId) {
		Point playerPos = playerPosMap.get(playerId);
		if(playerPos  != null) {
			availablePositions.add(playerPos);
			playerPosMap.remove(playerId);
		}
		playerScoreMap.remove(playerId);
	}
	
	

}