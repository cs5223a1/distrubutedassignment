

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class TrackerInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1944142349927174231L;
	private int n;
	private int k;
	private List<PlayerInfo> players;

	public TrackerInfo(int n, int k, List<PlayerInfo> players) {
		super();
		this.n = n;
		this.k = k;
		if(this.players == null) {
			this.players = new ArrayList<PlayerInfo>();
		}
		List<PlayerInfo> newList = new ArrayList<PlayerInfo>();
		if(players != null && !players.isEmpty()) {
			for(PlayerInfo player : players) {
				newList.add(player.clone());
			}
		}
		Collections.sort(newList,new TimeStampComparator());
		this.players = newList;
	}

	public int getN() {
		return n;
	}

	public int getK() {
		return k;
	}

	public void setPlayers( List<PlayerInfo> players ) {
			this.players = new LinkedList<PlayerInfo>(players);	
	}
	
	public List<PlayerInfo> getPlayers() {
		return players;
	}
}

class TimeStampComparator implements Comparator<PlayerInfo> {

	@Override
	public int compare(PlayerInfo p1, PlayerInfo p2) {
		if(p1.getTimeStamp() == p2.getTimeStamp())
			return 0;
		return p1.getTimeStamp() < p2.getTimeStamp() ? -1:1;
	}
}