import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ConcurrentModificationException;
import java.util.List;

public class PingDeadPlayers implements Runnable{


	private ITracker tr;
	private Tracker tcomp;
	private static int delay = 5000;
	public void init(Tracker t, int port)
	{
		Registry register;
		try {
			register = LocateRegistry.getRegistry(port);
			tr = (ITracker)register.lookup("tracker");
		} catch (RemoteException e1) {}
		catch (NotBoundException e) {}
		PingDeadPlayers target = new PingDeadPlayers(t, tr);
		while(true)
		{
			Thread th = new Thread(target);
			th.start();
			try {
				Thread.sleep(delay);
				delay= delay<10000?delay+500:delay + 1000;
				
			} catch (InterruptedException e) {
			}
		}
	}

	public PingDeadPlayers(Tracker t, ITracker tr) {
		this.tcomp = t;
		this.tr = tr;
	}

	public PingDeadPlayers() {
		// TODO Auto-generated constructor stub
	}

	//@Override
	public void run() {
		List<PlayerInfo> players = this.tcomp.getTrackerInfo().getPlayers();
		if(players == null) {
			return;
		}
		try
		{
			for(PlayerInfo p :players)
			{
				Registry register;
				try {
					if(p.isDead() == false)
					{
						register = LocateRegistry.getRegistry(p.getPort());
						IGame game = (IGame) register.lookup("player");
					}
				} catch (RemoteException e) {
					p.setDead(true);
				} catch (NotBoundException e) {
					p.setDead(true);
				}
			}
		}
		catch(ConcurrentModificationException e)
		{}
		try {
			if(players.size() > 0)
				tr.updatePlayerList(players);
		} catch (RemoteException e) {}		
	}
}