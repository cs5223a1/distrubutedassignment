

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GameLogic {
	private RandomGenerator randomGen;
	private GameState gameState;

	public GameLogic(GameState gameState) {
		this.gameState = gameState.clone();
		randomGen = new RandomGenerator();
		calcAvailablePositions();
		updateTreasures();
	}
	
	public void updatePrimaryServer(PlayerInfo player) {
		this.gameState.setPrimaryServer(player);
	}
	
	public void updateBackupServer(PlayerInfo player) {
		this.gameState.setBackUpServer(player);
	}
	
	public void setGameState(GameState gameState) {
		this.gameState = gameState.clone();
		calcAvailablePositions();
	}
	
	public PlayerInfo getPrimaryServer() {
		return this.gameState.primaryServer;
	}
	
	public PlayerInfo getBackUpServer() {
		return this.gameState.backUpServer;
	}

	private void calcAvailablePositions() {

		if (gameState.availablePositions == null) {
			gameState.availablePositions = new ArrayList<Point>();
		}
		for (int i = 0; i < gameState.n; i++) {
			for (int j = 0; j < gameState.n; j++) {
				Point p = new Point(i, j);
				if (!gameState.playerPosMap.values().contains(p) && !gameState.treasureLoc.contains(p)) {
					gameState.availablePositions.add(p);
				}
			}
		}
	}

	private void updateTreasures() {
		int treasureToCreate = gameState.k - gameState.treasureLoc.size();
		for (int i = 0; i < treasureToCreate; i++) {
			Point p = randomGen.getRandomPoint(gameState.availablePositions);
			gameState.treasureLoc.add(p);
			gameState.availablePositions.remove(p);
		}
	}

	public GameState addPlayer(String playerId) {
		PlayerState newPlayer = new PlayerState(playerId, randomGen.getRandomPoint(this.gameState.availablePositions), 0);
		gameState.playerPosMap.put(playerId, newPlayer.getPosition());
		gameState.playerScoreMap.put(playerId, 0);
		gameState.availablePositions.remove(newPlayer.getPosition());
		return gameState;
	}

	public void quitGame(String playerId) {

	}

	public void updatePlayerLocation(GameMove move, String playerId) {
		Point currPos = gameState.playerPosMap.get(playerId).getLocation();
		int newX = currPos.x, newY = currPos.y;
		if (move == GameMove.WEST) {
			newX -= 1;
		} else if (move == GameMove.EAST) {
			newX += 1;
		} else if (move == GameMove.NORTH) {
			newY -= 1;
		} else if (move == GameMove.SOUTH) {
			newY += 1;
		}
		if (newX < 0 || newY < 0 || newX == gameState.n || newY == gameState.n) {
			return;
		}
		Point p = new Point(newX, newY);
		if (!gameState.playerPosMap.values().contains(p)) {
			gameState.playerPosMap.put(playerId, p);
			gameState.availablePositions.add(currPos);
			gameState.availablePositions.remove(p);
			if (gameState.treasureLoc.contains(p)) {
				int currentScore = gameState.playerScoreMap.get(playerId);
				gameState.playerScoreMap.put(playerId, currentScore + 1);
				gameState.treasureLoc.remove(p);
				updateTreasures();
			}
		}
	}
	
	public GameState getGameState() {
		return this.gameState.clone();
	}
	
	public synchronized void removePlayer(String playerId) {
				this.gameState.removePlayer(playerId);
	}
}