import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class MazeUI extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static GameState gameState;

	static int x;
	static int y;
	static String playerId;
	static MyPanel pan;
	private MazeUI m;
	public MazeUI(GameState gameState,String playerId){
		MazeUI.gameState= gameState;
		this.setTitle(playerId);
		MazeUI.playerId = playerId;
		this.setSize(1000, 1000);
		this.setLocationRelativeTo(null);
		pan = new MyPanel();
		this.setContentPane(pan);               
		this.setVisible(true);
		this.m = this;
	}

	public MazeUI getM() {
		return m;
	}

	public void setM(MazeUI m) {
		this.m = m;
	}

	public void refresh(GameState gamestate)
	{
		MazeUI.gameState = gamestate;
		pan.repaint();
	}

}

class MyPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		
		GameState gameState = MazeUI.gameState;		
		//Grid
		g.setColor(Color.gray);
		for(int i=0;i<15;i++)
		{
			for(int j=0;j<15;j++)
			{
				g.drawRect(250+(j*50),30+(i*50) ,50,50);
			}
		}

		//Treasures
		Set<Point> treasures = gameState.treasureLoc;
		Iterator<Point> it = treasures.iterator();
		g.setColor(Color.darkGray);
		g.setFont(new Font("default", Font.BOLD, 25));
		while(it.hasNext())
		{
			Point coords = it.next();
			int x = coords.x;
			int y = coords.y;
			g.drawString("*", 250+(x*50) + 25, 30+(y*50) + 40);
		}
		//Players
		Map<String,Point> playerPos = gameState.playerPosMap;
		g.setColor(Color.RED);
		g.setFont(new Font("TimesRoman", Font.BOLD, 22));
		for (Map.Entry<String, Point> entry : playerPos.entrySet())
		{
			String playerId = entry.getKey();
			Point coords = entry.getValue();
			int x = coords.x;
			int y = coords.y;
			g.drawString(playerId, 250+(x*50) + 15, 30+(y*50) + 30);
		}

		//Servers
		g.setColor(Color.blue);
		g.setFont(new Font("TimesRoman", Font.BOLD, 15));
		String current_player = MazeUI.playerId;
		if(gameState.getPrimaryServer() != null 
				&& gameState.getPrimaryServer().getPlayerId().equals(current_player))
			g.drawString("Primary server", 50, 50);
		if(gameState.getBackUpServer() != null
				&& gameState.getBackUpServer().getPlayerId().equals(current_player) )
			g.drawString("Backup server", 50, 50);

		//PlayerList
		g.setColor(Color.black);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		int itr = 0;
		Map<String,Integer> playerscore = gameState.playerScoreMap;
		for (Map.Entry<String, Integer> entry : playerscore.entrySet())
		{
			String playerId = entry.getKey();
			int score = entry.getValue();
			g.drawString(playerId + " : " +score, 50, 100+(itr*30));
			itr++;
		}
	}
}