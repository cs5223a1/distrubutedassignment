

public enum GameMove {

	REFRESH((short)0),
	WEST((short)1),
	SOUTH((short)2),
	EAST((short)3),
	NORTH((short)4),
	QUIT((short)9);
	
	private short code;
	
	GameMove(short code) {
		this.code = code;
	}
	
	public short getCode() {
		return this.code;
	}
	
	public static GameMove getGameMode(String codeStr) {
		short code = Short.parseShort(codeStr);
		for(GameMove move : values()) {
			if(move.code == code) {
				return move;
			}
		}
		throw new IllegalArgumentException("Code not found!");
	}
}
