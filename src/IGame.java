

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

public interface IGame extends Remote {

	/**
	 * 1. Remove deadPlayers.
	 * 2. Join this player to the game.
	 * 3. Return game status
	 */
	GameState joinPrimaryAndUpdatePlayers(PlayerInfo server, PlayerInfo player, List<PlayerInfo /* playerIds */> deadPlayerIds) throws RemoteException;
	
	GameState joinBackupAndUpdatePlayers(PlayerInfo server, PlayerInfo player, List<PlayerInfo /* playerIds */> deadPlayerIds) throws RemoteException;

	/**
	 * Update game status as player makes a move
	 */
	GameState move(String move, String playerId) throws RemoteException;

	/**
	 * This call is made to the back up server, when primary server is not found
	 */
	void makePrimaryServer() throws RemoteException;
	
	/**
	 * This call is made to the back up server, when primary server is not found
	 */
	boolean makePrimaryServer(GameState state, PlayerInfo backupServer) throws RemoteException;
	
	/**
	 * The game state is sent to the backup server.
	 * @param gameState
	 * @throws RemoteException
	 */
	void copyState(GameState gameState)  throws RemoteException;

	void unbind(int port) throws RemoteException;
	
}
