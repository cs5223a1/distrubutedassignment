

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;

public class Tracker implements ITracker {

	private int n;
	private int k;
	private TrackerInfo t;
	public static void main(String[] args) {
		int port = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);
		int k = Integer.parseInt(args[2]);
		new Tracker().init(port,n,k);
	}
	
	public TrackerInfo getTrackerInfo() {
		return this.t;
	}
	
	

	@Override
	public synchronized TrackerInfo addAndFetchPlayers(PlayerInfo playerinfo) {		
		addPlayer(playerinfo);
		return t;
	}
	
	private void addPlayer(PlayerInfo playerInfo) {
		
		this.t.getPlayers().add(playerInfo);
	}
	

	@Override
	public  void init(int port,int n,int k) {
		Registry registry = null;
		try
		{
			try {
				registry = java.rmi.registry.LocateRegistry.createRegistry(port);
			} catch(ExportException e) {
				registry = LocateRegistry.getRegistry(port);
			}

			ITracker stub = (ITracker) UnicastRemoteObject.exportObject(this, 0);
			registry.rebind("tracker", stub);
			System.out.println("Tracker started. Port : " + port);
			this.n = n;
			this.k = k;
			this.t = new TrackerInfo(n, k, null);
			PingDeadPlayers p = new PingDeadPlayers();
			p.init(this, port);
		} catch (RemoteException e) {
		}
	}
	
	public void cleanRegistry(int port) {
		try {
			Registry registry = java.rmi.registry.LocateRegistry.createRegistry(port);
			if (registry != null) {
			    UnicastRemoteObject.unexportObject(registry, true);
			}
		} catch (RemoteException e) {
		}
		
	}

	@Override
	public void removePlayers(Set<String> playerIds) {
		List<PlayerInfo> playerInfoList = this.t.getPlayers();
		Iterator<PlayerInfo> iter = playerInfoList.iterator();

		while (iter.hasNext()) {
		    PlayerInfo p = iter.next();

		    if (playerIds.contains(p.getPlayerId())) {
		        iter.remove();
		    }
		}
		synchronized (playerInfoList) {
			this.t.setPlayers(playerInfoList);
		}
	}

	@Override
	public void removeAllPlayers() throws RemoteException {
		this.t = new TrackerInfo(t.getN(), t.getK(), new ArrayList<PlayerInfo>());
		System.out.println("[TrackerCompute] All Player were removed.");
	}



	@Override
	public void updatePlayerList(List<PlayerInfo> players) throws RemoteException {
		this.t.setPlayers(players);
	}
}