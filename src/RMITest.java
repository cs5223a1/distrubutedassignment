

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

public class RMITest implements IGame {

	public static void main(String[] args) {

		if(checkTracker(2000,"127.0.0.1")) {
			System.out.println("Tracker Alive");
		} else {
			System.out.println("Tracker Dead");
		}
		
		
		if(checkPort(2016)) {
			System.out.println("Primary Alive");
		} else {
			System.out.println("Primary Dead");
		}
		
		if(checkPort(2017)){
			System.out.println("Backup Alive");
		} else {
			System.out.println("Backup Dead");
		}
		
		if(checkPort(2020)){
			System.out.println("Player port Alive");
		} else {
			System.out.println("Player port Dead");
		}
	}
	

	private static boolean checkPort(int port) {
		Registry register;
		try {
			register = LocateRegistry.getRegistry(port);
			IGame g = (IGame) register.lookup("game");
		} catch (RemoteException e) {
			return false;
		} catch (NotBoundException e) {
			return false;
		}
		return true;
	}
	
	private static boolean checkTracker(int port,String host) {
		Registry register;
		try {
			register = LocateRegistry.getRegistry(port);
			//ITracker t = (ITracker)Naming.lookup("rmi://"+host+":"+port+"/IGame");
			Tracker g = (Tracker) register.lookup("tracker");
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		} catch (NotBoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public GameState joinPrimaryAndUpdatePlayers(PlayerInfo server, PlayerInfo player, List<PlayerInfo> deadPlayerIds)
			throws RemoteException {
		return null;
	}

	@Override
	public GameState joinBackupAndUpdatePlayers(PlayerInfo server, PlayerInfo player, List<PlayerInfo> deadPlayerIds)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameState move(String move, String playerId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void makePrimaryServer() throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean makePrimaryServer(GameState state, PlayerInfo backupServer) throws RemoteException {
		return false;
	}

	@Override
	public void copyState(GameState gameState) throws RemoteException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void unbind(int port) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

}
